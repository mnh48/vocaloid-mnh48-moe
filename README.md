# vocaloid.mnh48.moe
## What is this?
This is the repository of the files on vocaloid.mnh48.moe which is the home
page of MNH48's Vocaloid translations. The files on this subdomain serves
the translations of Vocaloid songs into Malay as well as the explanation
of the translations in English.

